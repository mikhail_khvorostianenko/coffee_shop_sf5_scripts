<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211009080157 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `order` (
            id INT AUTO_INCREMENT NOT NULL, 
            buyer VARCHAR(255) NOT NULL, 
            phone VARCHAR(50) NOT NULL, 
            delivery VARCHAR(255) NOT NULL, 
            details LONGTEXT NOT NULL, 
            total DOUBLE PRECISION NOT NULL, 
            created_at DATETIME NOT NULL, 
            completed_at DATETIME DEFAULT NULL, 
            status INT NOT NULL, 
            PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE `order`');
    }
}
