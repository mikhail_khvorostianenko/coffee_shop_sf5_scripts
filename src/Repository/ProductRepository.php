<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use PDO;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findRecentSortedProducts(string $search, int $maxResults): mixed
    {
        $qb = $this->createQueryBuilder('product')
            ->andWhere('product.enabled = TRUE')
            ->addOrderBy('product.id', 'DESC');

        if ($search) {
            $qb->andWhere('product.name LIKE :search OR product.description LIKE :search')
                ->setParameter('search', "%{$search}%");
        }

        $qb->setMaxResults($maxResults);

        return $qb->getQuery()->getResult();
    }

    public function findSameCategoryProducts(Product $product, int $limit, array $except): mixed
    {
        $qb = $this->createQueryBuilder('product')
            ->andWhere('product.category = :category')
            ->andWhere('product != :product')
            ->setParameter('category', $product->getCategory())
            ->setParameter('product', $product)
            ->setMaxResults($limit);

        if ($except) {
            $this->addExceptProducts($qb, $except);
        }

        return $qb->getQuery()->getResult();
    }

    private function addExceptProducts(QueryBuilder $qb, array $productIds)
    {
        $qb->andWhere($qb->expr()->notIn('product.id', $productIds));
    }

    public function findOtherCategoryProducts(Product $product, int $limit, array $except): mixed
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = "SELECT id
                FROM product
                WHERE category_id != :category
                ORDER BY RAND() 
                LIMIT :limit";
        $statement = $connection->prepare($sql);

        $statement->bindValue('category', $product->getCategory()->getId(), PDO::PARAM_INT);
        $statement->bindValue('limit', $limit, PDO::PARAM_INT);

        $result = $statement->executeQuery();
        $randomIds = $result->fetchFirstColumn();

        $qb = $this->createQueryBuilder('product')
            ->andWhere('product.id IN (:random_ids)')
            ->setParameter('random_ids', $randomIds)
            ->setMaxResults($limit);

        if ($except) {
            $this->addExceptProducts($qb, $except);
        }

        return $qb->getQuery()->getResult();
    }


    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
