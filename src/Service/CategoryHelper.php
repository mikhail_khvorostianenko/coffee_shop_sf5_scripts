<?php

namespace App\Service;

class CategoryHelper
{
    public function transformStringToSlug(string $string): string
    {
        $words = explode(' ', $string);
        // delete all empty values
        $words = array_filter($words);
        $words = array_map('mb_strtolower', $words);

        return join('-', $words);
    }
}
