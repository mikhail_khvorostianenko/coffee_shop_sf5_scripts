<?php

namespace App\Service;

use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;

class CartHelper
{
    public function getCurrentCart(Request $request): array
    {
        return $request->cookies->has('cart') ? json_decode($request->cookies->get('cart'), true) : [];
    }

    /**
     * @param Request $request
     * @param Product[] $products
     * @return void
     */
    public function markProductsAlreadyAddedToCart(Request $request, array $products): void
    {
        $cart = $this->getCurrentCart($request);

        foreach ($products as $product) {
            if (empty($cart[$product->getId()])) {
                continue;
            }

            $product->setAlreadyInCart(true);
        }
    }
}