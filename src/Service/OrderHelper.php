<?php

namespace App\Service;

use App\Entity\Product;

class OrderHelper
{
    public function productDetailsSuitableForOrder(Product $product, int $quantity): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'image' => $product->getImage(),
            'quantity' => $quantity,
        ];
    }
}
