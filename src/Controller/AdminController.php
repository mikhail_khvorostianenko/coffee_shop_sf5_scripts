<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin_homepage')]
    public function index(OrderRepository $orderRepository, ProductRepository $productRepository): Response
    {
        $numberOfProducts = $productRepository->count([]);
        $numberOfNewOrders = $orderRepository->count(['status' => Order::STATUS_NEW]);
        $lastNewOrder = $orderRepository->findOneBy(['status' => Order::STATUS_NEW], ['createdAt' => 'DESC']);

        return $this->render('admin/index.html.twig', [
            'last_order' => $lastNewOrder,
            'number_of_products' => $numberOfProducts,
            'number_of_new_orders' => $numberOfNewOrders,
        ]);
    }
}
