<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductAdminController extends AbstractController
{
    private CategoryRepository $categoryRepository;
    private FileUploader $fileUploader;

    public function __construct(CategoryRepository $categoryRepository, FileUploader $fileUploader)
    {
        $this->categoryRepository = $categoryRepository;
        $this->fileUploader = $fileUploader;
    }

    #[Route('/admin/product', name: 'admin_product_list')]
    public function list(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();

        return $this->render('admin/product/list.html.twig', ['products' => $products]);
    }

    #[Route('/admin/product/new', name: 'admin_product_new')]
    public function new(EntityManagerInterface $entityManager, Request $request): Response
    {
        if ($request->getMethod() === 'POST') {
            list($name, $description, $price, $enabled, $category, $fileName) = $this->processForm($request);

            $product = new Product();

            $product->setName($name)
                ->setDescription($description)
                ->setPrice($price)
                ->setEnabled($enabled)
                ->setCategory($category)
                ->setImage($fileName);

            $entityManager->persist($product);
            $entityManager->flush();
            $this->addFlash('success', 'Product successfully created');

            return $this->redirectToRoute('admin_product_list');
        }

        $categories = $this->categoryRepository->findAll();

        return $this->render('admin/product/new.html.twig', ['categories' => $categories]);
    }

    #[Route('/admin/product/delete/{id}', name: 'admin_product_delete')]
    public function delete(Product $product, EntityManagerInterface $entityManager): Response
    {
        $imageFullPath = $this->fileUploader->getTargetDirectory()."/".$product->getImage();
        unlink($imageFullPath);

        $entityManager->remove($product);
        $entityManager->flush();

        $this->addFlash('success', 'Product successfully deleted');

        return $this->redirectToRoute('admin_product_list');
    }

    #[Route('/admin/product/edit/{id}', name: 'admin_product_edit')]
    public function edit(Request $request, EntityManagerInterface $entityManager, Product $product): Response
    {
        if ($request->getMethod() === 'POST') {
            list($name, $description, $price, $enabled, $category, $fileName) = $this->processForm($request);

            $product->setName($name)
                ->setDescription($description)
                ->setPrice($price)
                ->setEnabled($enabled)
                ->setCategory($category);

            if ($fileName) {
                if ($product->getImage()) {
                    unlink($this->getParameter('product_images_directory') . '/' . $product->getImage());
                }

                $product->setImage($fileName);
            }

            $entityManager->flush();
            $this->addFlash('success', 'Product successfully saved');
        }

        $categories = $this->categoryRepository->findAll();

        return $this->render('admin/product/edit.html.twig', ['product' => $product, 'categories' => $categories]);
    }

    private function processForm(Request $request): array
    {
        $name = $request->request->get('name');
        $description = $request->request->get('description');
        $price = (float)$request->request->get('price');
        $enabled = (bool)$request->request->get('enabled');

        $categoryId = $request->request->get('category');
        $category = $this->categoryRepository->find($categoryId);

        $fileName = $request->files->get('file')
            ? $this->fileUploader->upload($request->files->get('file'))
            : '';
        return [$name, $description, $price, $enabled, $category, $fileName];
    }
}