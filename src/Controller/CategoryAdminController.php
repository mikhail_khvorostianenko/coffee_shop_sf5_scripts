<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Service\CategoryHelper;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryAdminController extends AbstractController
{
    private CategoryHelper $categoryHelper;
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryHelper $categoryHelper, CategoryRepository $categoryRepository)
    {
        $this->categoryHelper = $categoryHelper;
        $this->categoryRepository = $categoryRepository;
    }

    #[Route('/admin/category', name: 'admin_category_list')]
    public function list(CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();

        return $this->render('admin/category/list.html.twig', [
            'categories' => $categories,
        ]);
    }

    #[Route('/admin/category/new', name: 'admin_category_new')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        if ($request->getMethod() === 'POST') {
            list($name, $enabled, $priority, $slug, $errorMessage) = $this->processForm($request);

            if ($errorMessage) {
                $this->addFlash('error', $errorMessage);
                return $this->redirectToRoute('admin_category_new');
            }

            $category = new Category();
            $category->setName($name)
                ->setSlug($slug)
                ->setEnabled($enabled)
                ->setPriority($priority);

            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash('success', 'Category successfully created');

            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/category/new.html.twig');
    }

    #[Route('/admin/category/edit/{slug}', name: 'admin_category_edit')]
    public function edit(Category $category, Request $request, EntityManagerInterface $entityManager): Response
    {
        if ($request->getMethod() === 'POST') {
            list($name, $enabled, $priority, $slug, $errorMessage) = $this->processForm($request, $category);

            if ($errorMessage) {
                $this->addFlash('error', $errorMessage);
                return $this->redirectToRoute('admin_category_edit', ['slug' => $category->getSlug()]);
            }

            $category->setName($name)
                ->setSlug($slug)
                ->setEnabled($enabled)
                ->setPriority($priority);

            $entityManager->flush();

            $this->addFlash('success', 'Category successfully saved');

            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/category/edit.html.twig', ['category' => $category]);
    }

    #[Route('/admin/category/delete/{slug}', name: 'admin_category_delete')]
    public function delete(Category $category, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($category);
        $entityManager->flush();

        $this->addFlash('success', 'Category successfully deleted');

        return $this->redirectToRoute('admin_category_list');
    }

    private function processForm(Request $request, ?Category $category = null): array
    {
        $name = $request->request->get('name');
        $enabled = (bool) $request->request->get('enabled');
        $priority = (int) $request->request->get('priority');
        $slug = $this->categoryHelper->transformStringToSlug($name);
        $errorMessage = '';

        $isNameUnique = $this->categoryRepository->isNameUnique($name, $category);
        if (!$isNameUnique) {
            $errorMessage = "Name `{$name}` is not unique";
        }

        return [$name, $enabled, $priority, $slug, $errorMessage];
    }
}
