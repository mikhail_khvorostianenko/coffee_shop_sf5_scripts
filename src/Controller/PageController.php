<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Service\CartHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    const ALTERNATIVE_PRODUCTS_LIMIT = 3;

    private CartHelper $cartHelper;

    public function __construct(CartHelper $cartHelper)
    {
        $this->cartHelper = $cartHelper;
    }

    #[Route('/', name: 'homepage')]
    public function index(ProductRepository $productRepository, Request $request): Response
    {
        $search = (string)$request->query->get('search');
        $recentProducts = $productRepository->findRecentSortedProducts($search, 12);
        $this->cartHelper->markProductsAlreadyAddedToCart($request, $recentProducts);

        return $this->render('front/pages/main.html.twig', ['products' => $recentProducts, 'search' => $search]);
    }

    public function categoryList(CategoryRepository $categoryRepository): Response
    {
        return $this->render(
            'blocks/categories.html.twig',
            ['categories' => $categoryRepository->findEnabledWithActiveProductsOrderedByPriority()]
        );
    }

    #[Route('/category/{slug}', name: 'category')]
    public function category(Request $request, Category $category, ProductRepository $productRepository): Response
    {
        $products = $productRepository->findBy(['category' => $category, 'enabled' => true], ['id' => 'DESC']);
        $this->cartHelper->markProductsAlreadyAddedToCart($request, $products);

        return $this->render('front/pages/category.html.twig', ['products' => $products, 'category' => $category]);
    }

    #[Route('/product/{id}', name: 'product')]
    public function product(Request $request, Product $product, ProductRepository $productRepository): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);
        $except = array_keys($cart);


        $sameCategoryProducts = $productRepository->findSameCategoryProducts(
            $product,
            self::ALTERNATIVE_PRODUCTS_LIMIT,
            $except
        );
        $otherCategoryProducts = $productRepository->findOtherCategoryProducts(
            $product,
            self::ALTERNATIVE_PRODUCTS_LIMIT,
            $except
        );
        $this->cartHelper->markProductsAlreadyAddedToCart($request, [$product]);

        return $this->render('front/pages/product.html.twig', [
            'product' => $product,
            'same_category_products' => $sameCategoryProducts,
            'other_category_products' => $otherCategoryProducts,
        ]);
    }


    #[Route('/delivery', name: 'delivery')]
    public function delivery(): Response
    {
        return $this->render('front/pages/delivery.html.twig');
    }

    #[Route('/contacts', name: 'contacts')]
    public function contacts(): Response
    {
        return $this->render('front/pages/contacts.html.twig');
    }
}