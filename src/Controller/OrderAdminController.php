<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTimeImmutable;

class OrderAdminController extends AbstractController
{
    const COUNT_PER_PAGE = 10;

    #[Route('/admin/order', name: 'admin_order_list')]
    public function list(Request $request, OrderRepository $orderRepository): Response
    {
        $selectedPageNumber = $request->get('page') ?: 1;
        $count = $orderRepository->count([]);
        $total = ceil($count/self::COUNT_PER_PAGE);

        $orders = $orderRepository->findBy(
            [],
            ['status' => 'ASC', 'completedAt' => 'DESC', 'createdAt' => 'DESC'],
            self::COUNT_PER_PAGE,
            ($selectedPageNumber - 1) * self::COUNT_PER_PAGE
        );

        $pagination = [
            'hasNext' => $selectedPageNumber < $total,
            'hasPrev' => $selectedPageNumber > 1,
            'total' => $total,
            'current' => $selectedPageNumber,
        ];

        return $this->render('admin/order/list.html.twig', [
            'orders' => $orders,
            'statuses' => Order::STATUSES,
            'pagination' => $pagination,
        ]);
    }


    #[Route('/admin/order/details/{id}', name: 'admin_order_details')]
    public function details(Request $request, EntityManagerInterface $entityManager, Order $order): Response
    {
        if ($request->getMethod() === 'POST') {
            $newStatus = (int)$request->request->get('status');
            $oldStatus = $order->getStatus();

            if ($oldStatus !== $newStatus) {
                if ($order->getCompletedAt() && $newStatus !== Order::STATUS_COMPLETED) {
                    $order->setCompletedAt(null);
                }

                if ($newStatus === Order::STATUS_COMPLETED) {
                    $order->setCompletedAt(new DateTimeImmutable());
                }

                $order->setStatus($newStatus);
                $entityManager->flush();

                $this->addFlash(
                    'success',
                    'Order #'.$order->getId().' status updated from "'.Order::STATUSES[$oldStatus].'" to "'.Order::STATUSES[$newStatus].'"'
                );
            }

            return $this->redirectToRoute('admin_order_list');
        }

        return $this->render('admin/order/details.html.twig', ['order' => $order, 'statuses' => Order::STATUSES]);
    }

    #[Route('/admin/order/delete/{id}', name: 'admin_order_delete')]
    public function delete(Order $order, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($order);
        $entityManager->flush();

        $this->addFlash('success', 'Order successfully deleted');

        return $this->redirectToRoute('admin_order_list');
    }
}
