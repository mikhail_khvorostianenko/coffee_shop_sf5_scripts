<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Service\CartHelper;
use App\Service\OrderHelper;
use DateTimeImmutable;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    private CartHelper $cartHelper;
    private ProductRepository $productRepository;

    public function __construct(CartHelper $cartHelper, ProductRepository $productRepository)
    {
        $this->cartHelper = $cartHelper;
        $this->productRepository = $productRepository;
    }

    #[Route("/cart/add/{id}", name: "add_to_cart")]
    public function add(Product $product, Request $request): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);
        $cart[$product->getId()] = !empty($cart[$product->getId()]) ? ++$cart[$product->getId()] : 1;

        $referer = $request->headers->get('referer');
        $response = $this->redirect($referer);
        $response->headers->setCookie(Cookie::create('cart', json_encode($cart)));

        return $response;
    }

    #[Route("/cart/checkout", name: "cart_checkout")]
    public function checkout(Request $request): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);

        if (!$cart) {
            return $this->redirectToRoute('homepage');
        }

        $response = new Response();
        if ($request->getMethod() === 'POST') {
            $updatedProductsData = $request->request->get('products') ?? [];

            foreach ($updatedProductsData as $productId => $quantity) {
                if ($quantity) {
                    $cart[$productId] = (int)$quantity;
                    continue;
                }

                unset($cart[$productId]);
            }

            $response->headers->setCookie(Cookie::create('cart', json_encode($cart)));
        }

        $orderProducts = $this->getOrderProducts($cart);

        return $this->render(
            'front/cart/checkout.html.twig',
            [
                'order_products' => $orderProducts,
                'total_cost' => $this->calculateTotalCost($cart),
                'no_need_mini_cart' => true,
            ],
            $response
        );
    }

    /**
     * @param array $cart
     * @return Product[]
     */
    private function getOrderProducts(array $cart): array
    {
        $orderProducts = $this->productRepository->findBy(['id' => array_keys($cart)]);
        foreach ($orderProducts as $orderProduct) {
            $productQuantity = $cart[$orderProduct->getId()];
            $orderProduct->setOrderQuantity($productQuantity);
        }

        return $orderProducts;
    }

    #[Route("cart/delete/{id}", name: 'delete_from_cart')]
    public function delete(Product $product, Request $request): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);

        if (!empty($cart[$product->getId()])) {
            unset($cart[$product->getId()]);
        }

        $referer = $request->headers->get('referer');
        $response = $this->redirect($referer);
        $response->headers->setCookie(Cookie::create('cart', json_encode($cart)));

        return $response;
    }

    #[Route("/cart/delivery", name: "cart_delivery")]
    public function delivery(Request $request, OrderHelper $orderHelper): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);

        if (!$cart) {
            return $this->redirectToRoute('homepage');
        }

        if ($request->getMethod() === 'POST') {
            $buyerName = $request->request->get('name');
            $phone = $request->request->get('phone');
            $deliveryAddress = $request->request->get('delivery');

            $cartProducts = $this->productRepository->findBy(['id' => array_keys($cart)]);

            $total = 0;
            $orderProducts = [];
            foreach ($cartProducts as $cartProduct) {
                $quantity = $cart[$cartProduct->getId()];
                $total += $cartProduct->getPrice() * $quantity;
                $orderProducts[] = $orderHelper->productDetailsSuitableForOrder($cartProduct, $quantity);
            }

            $order = new Order();
            $order->setBuyer($buyerName)
                ->setPhone($phone)
                ->setDelivery($deliveryAddress)
                ->setCreatedAt(new DateTimeImmutable())
                ->setDetails(json_encode($orderProducts))
                ->setTotal($total)
                ->setStatus(Order::STATUS_NEW);

            $em = $this->getDoctrine()->getManager();
            $em->persist($order);
            $em->flush();

            $response = new Response();
            $response->headers->setCookie(Cookie::create('cart', ''));

            return $this->render(
                'front/cart/success.html.twig',
                [
                    'order' => $order,
                    'no_need_mini_cart' => true,
                ],
                $response
            );
        }

        $orderProducts = $this->getOrderProducts($cart);

        return $this->render(
            'front/cart/delivery.html.twig',
            [
                'total_cost' => $this->calculateTotalCost($cart),
                'no_need_mini_cart' => true,
                'edit_cart_disabled' => true,
                'order_products' => $orderProducts,
            ]
        );
    }

    public function mini(Request $request): Response
    {
        $cart = $this->cartHelper->getCurrentCart($request);
        $totalCost = $uniqueProductsCount = 0;

        if ($cart) {
            $cartProducts = $this->productRepository->findBy(['id' => array_keys($cart)]);

            $uniqueProductsCount = count($cartProducts);
            $totalCost = $this->calculateTotalCost($cart);
        }

        return $this->render(
            'front/cart/mini.html.twig',
            ['unique_products_count' => $uniqueProductsCount, 'total_cost' => $totalCost]
        );
    }

    private function calculateTotalCost(array $cart): float
    {
        $totalCost = 0;

        $cartProducts = $this->productRepository->findBy(['id' => array_keys($cart)]);
        foreach ($cartProducts as $cartProduct) {
            $productQuantity = $cart[$cartProduct->getId()];
            $totalCost += $cartProduct->getPrice() * $productQuantity;
        }

        return $totalCost;
    }
}