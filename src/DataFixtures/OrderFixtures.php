<?php

namespace App\DataFixtures;

use App\Entity\Order;
use App\Entity\Product;
use App\Service\OrderHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture implements DependentFixtureInterface
{
    private const ORDERS_COUNT = 100;

    private OrderHelper $orderHelper;

    public function __construct(OrderHelper $orderHelper)
    {
        $this->orderHelper = $orderHelper;
    }

    public function load(ObjectManager $manager): void
    {
        $products = $this->getProducts();

        $now = new \DateTimeImmutable();
        foreach (range(1, self::ORDERS_COUNT) as $number) {
            list($orderProducts, $total) = $this->getOrderDetails($products);

            $days = rand(4,9);
            $createdAt = $now->modify("-{$days} day");
            $status = array_rand(Order::STATUSES);

            $order = new Order();
            $order->setBuyer('Buyer name #'.$number)
                ->setPhone(rand(10000000,99999999))
                ->setDelivery('Delivery address #'.$number)
                ->setCreatedAt($createdAt)
                ->setDetails(json_encode($orderProducts))
                ->setTotal($total)
                ->setStatus($status);

            if ($status === Order::STATUS_COMPLETED) {
                $days = rand(1,3);
                $completedAt = $createdAt->modify("+{$days} days");
                $order->setCompletedAt($completedAt);
            }

            $manager->persist($order);
        }

        $manager->flush();
    }

    private function getProducts(): array
    {
        $products = [];
        for ($i = 0; true; $i++) {
            if (!$this->hasReference('product_' . $i)) {
                break;
            }

            $products[] = $this->getReference('product_' . $i);
        }

        return $products;
    }

    private function getOrderDetails(array $products): array
    {
        $randProductKeys = array_rand($products, rand(2,5));
        $orderProducts = [];
        $total = 0;

        foreach ($randProductKeys as $key) {
            /** @var Product $randProduct */
            $randProduct = $products[$key];
            $quantity = rand(1, 5);
            $total += $randProduct->getPrice() * $quantity;

            $orderProducts[] = $this->orderHelper->productDetailsSuitableForOrder($randProduct, $quantity);
        }

        return [$orderProducts, $total];
    }

    public function getDependencies(): array
    {
        return [ProductFixtures::class];
    }
}
