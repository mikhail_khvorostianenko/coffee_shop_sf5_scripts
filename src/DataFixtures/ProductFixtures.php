<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Service\FileUploader;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    const PRODUCTS_BY_CATEGORY = [
        CategoryFixtures::GROUND_COFFEE => ['count' => 4, 'image' => 'category_'.CategoryFixtures::GROUND_COFFEE.'.png'],
        CategoryFixtures::BEANS_COFFEE => ['count' => 5, 'image' => 'category_'.CategoryFixtures::BEANS_COFFEE.'.png'],
        CategoryFixtures::TEA => ['count' => 6, 'image' => 'category_'.CategoryFixtures::TEA.'.png'],
    ];

    const DESCRIPTION_POSTFIX = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vitae nunc quam.
        Morbi tincidunt ipsum ac ligula hendrerit ornare. Nam tincidunt convallis arcu, finibus consequat magna
        tristique in. Nulla leo magna, euismod quis est eu, placerat tempor arcu. Suspendisse lacinia eros id porta
        blandit. Morbi quis ante hendrerit, dignissim turpis non, sodales ante. Nunc facilisis mollis lorem eu
        fermentum. Sed rhoncus, orci a tristique tempus, nisl orci bibendum arcu, nec fringilla purus tellus tristique
        arcu. Sed in justo quis sem imperdiet sollicitudin.';

    private FileUploader $fileUploader;

    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    public function load(ObjectManager $manager)
    {
        $referenceId = 0;
        foreach (self::PRODUCTS_BY_CATEGORY as $categoryKey => $productsData) {
            /** @var Category $category */
            $category = $this->getReference('category_'.$categoryKey);
            $categoryImageName = "category_{$categoryKey}";

            for ($index = 1; $index <= $productsData['count']; $index++) {
                $name = "{$category->getName()} #{$index}";
                $description = "{$category->getName()} #{$index} product description. ".self::DESCRIPTION_POSTFIX;

                copy(
                    __DIR__."/images/products/{$categoryImageName}.png",
                    __DIR__."/images/products/{$categoryImageName}_copy.png"
                );

                $productImageName = "{$categoryImageName}_{$index}";

                $file = new UploadedFile(
                    __DIR__."/images/products/{$categoryImageName}_copy.png",
                    $productImageName,
                    null,
                    null,
                    true
                );

                $image = $this->fileUploader->upload($file);

                $product = new Product();
                $product->setName($name)
                    ->setDescription($description)
                    ->setPrice(rand(10, 20))
                    ->setEnabled(true)
                    ->setImage($image)
                    ->setCategory($category);
                $manager->persist($product);

                $this->addReference('product_'.$referenceId, $product);
                $referenceId++;
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [CategoryFixtures::class];
    }
}
