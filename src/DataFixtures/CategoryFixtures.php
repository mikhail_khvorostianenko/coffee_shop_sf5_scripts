<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Service\CategoryHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    const GROUND_COFFEE = 1;
    const BEANS_COFFEE = 2;
    const TEA = 3;

    const CATEGORIES = [
        self::GROUND_COFFEE => 'Ground coffee',
        self::BEANS_COFFEE => 'Beans coffee',
        self::TEA => 'Tea',
    ];

    private CategoryHelper $categoryHelper;

    public function __construct(CategoryHelper $categoryHelper)
    {
        $this->categoryHelper = $categoryHelper;
    }

    public function load(ObjectManager $manager)
    {
        $priority = 1;

        foreach (self::CATEGORIES as $categoryKey => $categoryName) {
            $slug  = $this->categoryHelper->transformStringToSlug($categoryName);

            $category = new Category();
            $category->setName($categoryName)
                ->setSlug($slug)
                ->setPriority($priority)
                ->setEnabled(true);
            $manager->persist($category);
            $priority++;

            $this->addReference('category_'.$categoryKey, $category);
        }

        $manager->flush();
    }
}
